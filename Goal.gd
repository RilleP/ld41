extends Area2D

var puck;

export(int) var teamIndex = 0;

var goalie;

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass


func _physics_process(delta):
	if puck != null:
		var p = puck.position - $GoalLine.global_position;
		p = p.rotated(rotation);
		if p.y >= puck.radius:
			get_tree().get_root().get_node("Game").on_goal(teamIndex);
			puck.didScore = true;
			

func _on_Goal_area_entered(area):
	if area.get("radius") != null:
		self.puck = area;
		print("Goal entered " + str(area.radius));
	
func _on_Goal_area_exited(area):
	if area == puck:
		puck = null;
