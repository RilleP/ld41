extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var borderLines = [];

func _ready():
	return;
	var poly = $CollisionPolygon2D.polygon;
	
	var border = Area2D.new();
	border.set_collision_layer_bit(4, true);
	add_child(border);
	var pp = poly[poly.size()-1];
	for p in poly:
		var line = LineShape2D.new();
		var v = p - pp;
		var start = pp;
		var end = p;
		var dx = end.x-start.x;
		var dy = end.y-start.y;
		line.normal = Vector2(-dy, dx);
		var shape = CollisionShape2D.new();
		shape.shape = line;
		shape.scale = Vector2(dx, dy);
		shape.position = Vector2(dx*0.5, dy*0.5);
		
		border.add_child(shape);

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
