extends Node2D

const levelLayerBit = 0;
const playerLayerBit = 1;
const puckLayerBit = 2;

onready var level = get_node("../Level");

var rangeIndicator;
const rangeIndicatorBaseRadius = 100;
const rangeIndicatorColorMyTeam = Color(0, 0, 1, 0.2);
const rangeIndicatorColorOtherTeam = Color(1, 1, 0, 0.2);

var selectedPlayer setget select_player;

var myTeamIndex = 0;

export(bool) var multiplayer = true;

onready var game = get_tree().get_root().get_node("Game");

func select_player(player):
	if selectedPlayer != null:
		selectedPlayer.get_node("SelectedIndicator").visible = false;
	
	selectedPlayer = player;
	
	
	if selectedPlayer != null:
		selectedPlayer.get_node("SelectedIndicator").visible = true;
		update_range_idicator();
		
	update_move_points_label();		
		
func update_move_points_label():
	var movePointsLabel = get_node("../UI/MovePointsLabel");
	
	if selectedPlayer != null:
		selectedPlayer.update_move_points_label(movePointsLabel);
	else:
		movePointsLabel.text = "";
		
func update_moves_left_label():
	var visibleLabel = get_node("../UI/TurnMovesLabel"+str(game.curTeamIndexTurn));
	visibleLabel.text = "Moves left: " + str(game.curTeamMovesRemaining);
	visibleLabel.visible = true;
	
	var otherLabel = get_node("../UI/TurnMovesLabel"+str((game.curTeamIndexTurn+1)%2));
	otherLabel.visible = false;
	
func update_range_idicator():
	var player = selectedPlayer;
	
	if player != null && player.can_move():
		if player.teamIndex == myTeamIndex:
			rangeIndicator.color = rangeIndicatorColorMyTeam;
		else:
			rangeIndicator.color = rangeIndicatorColorOtherTeam;
		rangeIndicator.position = player.position;
		rangeIndicator.visible = true;
		var scale = player.get_move_range() / rangeIndicatorBaseRadius;
		rangeIndicator.scale = Vector2(scale, scale);
	else:
		rangeIndicator.visible = false;
	

func _ready():
	rangeIndicator = Polygon2D.new();
	rangeIndicator.color = Color(0, 0, 1, 0.2);
	update_range_idicator();
	var polygon = [];
	var radius = rangeIndicatorBaseRadius;
	var count = int(radius);	
	var distance = PI*2 / count;
	
	for i in range(count):
		var angle = i * distance;
		polygon.append(Vector2(cos(angle)*radius, sin(angle)*radius));
	
	rangeIndicator.polygon = polygon;
	add_child(rangeIndicator);
	game.connect("on_new_turn", self, "_on_new_turn");
	game.connect("on_remaining_moves_changed", self, "_on_remaining_moves_changed");
	
func _on_remaining_moves_changed(teamIndex, remainingMoves):
	update_moves_left_label();
	
func _on_new_turn(teamIndexTurn):
	if multiplayer:
		self.myTeamIndex = teamIndexTurn;
	update_move_points_label()
	update_range_idicator();
	

	
func _input(event):
	var levelAndPlayerMask = (1 << levelLayerBit | 1 << playerLayerBit)
	if event is InputEventMouseButton:
		if event.button_mask == BUTTON_LEFT:
			var first = get_first_hit_at_point(event.position, levelAndPlayerMask);
			if first != null:
				if first.get_collision_layer_bit(playerLayerBit):
					self.selectedPlayer = first;
				if first.get_collision_layer_bit(levelLayerBit):
					if can_controll_selected_player() && selectedPlayer.puck != null:
						if !(game.curTeamGoalieHasPuck && !(selectedPlayer.is_in_group("Goalie"))):
							selectedPlayer.shoot(event.position);
							update_range_idicator();
							update_move_points_label();
						else:
							game.show_warning("Must shoot with goalie");
						
		elif event.button_mask == BUTTON_RIGHT:
			var first = get_first_hit_at_point(event.position, levelAndPlayerMask);
			if first != null:
				print(first.name);
				if first.get_collision_layer_bit(playerLayerBit):
					if can_controll_selected_player():
						move_player(selectedPlayer, event.position);
				if first.get_collision_layer_bit(levelLayerBit):
					if can_controll_selected_player():
						move_player(selectedPlayer, event.position);
				if first.get_collision_layer_bit(puckLayerBit):
					if can_controll_selected_player():
						move_player(selectedPlayer, event.position);

func can_controll_selected_player():
	return selectedPlayer != null && game.curTeamIndexTurn == myTeamIndex && selectedPlayer.teamIndex == myTeamIndex;

func move_player(player, dest):
	if game.curTeamGoalieHasPuck:
		game.show_warning("Must shoot with goalie");
		return;
		
	if player.teamIndex != myTeamIndex || !player.can_move():
		return;
		
	var moveRange = player.get_move_range();
	var isGoalie = moveRange < 0;
	
	
	if isGoalie:
		var delta = 0;
		if dest.y > player.position.y + 32:
			delta = 1;
		elif dest.y < player.position.y - 32:
			delta = -1;
		if delta == 0:
			return;
		player.move(delta);
	elif player.position.distance_to(dest) <= player.get_move_range():
		player.move_to(dest);
	update_range_idicator();
	
	if player == selectedPlayer:
		update_move_points_label();
					
func get_first_hit_at_point(point, layerMask):
	var space = get_world_2d().direct_space_state;
	var hits = space.intersect_point(point, 32, [], layerMask);
	print(str(hits.size()) + " hits");
	if !hits.empty():
		var result;
		var resultDist;
		for hit in hits:
			var col = hit["collider"];
			
			var dist = col.position.distance_to(point);
			if col.get_collision_layer_bit(playerLayerBit):
				dist -= 10;
				if col.teamIndex == myTeamIndex:
					dist -= 10;			
			
			if result == null || resultDist > dist:
				result = col;
				resultDist = dist;
		
		return result;
	return null;
	

