extends Area2D

onready var game = get_tree().get_root().get_node("Game");

var puck = null setget set_puck;

var firstMoveUpdate = false;
var moving = false;
var destination;
var startPosition;
var prevPosition;

var maxMovePoints = 4.0;
var movePointsPerTurn = 0.25;
var movePoints = maxMovePoints setget _set_movePoints;
func _set_movePoints(newValue):
	movePoints = newValue;
	$MovePointsBar.value = movePoints;

var movedThisTurn = false;

export(int) var teamIndex = 0;

var spriteRotation = 0;

var stunTurns = 0;

const puckOffsets = [
	Vector2(-5, 40),
	Vector2(-40, 0),
	Vector2(30, -20),
	Vector2(40, 20),
];

const idleSpritesBlue = [
	preload("res://player/idle_0.png"),
	preload("res://player/idle_1.png"),
	preload("res://player/idle_2.png"),
	preload("res://player/idle_3.png"),
]

const idleSpritesRed = [
	preload("res://player/red_idle_0.png"),
	preload("res://player/red_idle_1.png"),
	preload("res://player/red_idle_2.png"),
	preload("res://player/red_idle_3.png"),
]

const shootAudio = preload("res://audio/shoot1_random.tres");
const takeAudio = preload("res://audio/take_random.tres");
const tackleAudio = preload("res://audio/tackle.wav");

const stunnedSpriteRed = preload("res://player/red_stunned.png");
const stunnedSpriteBlue = preload("res://player/stunned.png");

var idleSprites;
var stunnedSprite;

func set_puck(newValue):
	if puck != null:
		puck.ownedBy = null;
	puck = newValue;
	if puck != null:
		puck.ownedBy = self;
		$ShootAudioPlayer.stream = takeAudio;
		$ShootAudioPlayer.play();
		if puck.moving:
			puck.moving = false;
			game.remove_action_object(puck);
			
		puck.position = position + puckOffsets[spriteRotation];
		
func _ready():
	if teamIndex == 0:
		idleSprites = idleSpritesBlue;
		stunnedSprite = stunnedSpriteBlue;
	else:
		idleSprites = idleSpritesRed;
		stunnedSprite = stunnedSpriteRed;
	$Sprite.texture = idleSprites[spriteRotation];
	game.connect("on_new_turn", self, "_on_new_turn");
	startPosition = position;
	
func reset():
	self.position = startPosition;
	self.puck = null;
	self.movePoints = maxMovePoints;
	self.movedThisTurn = false;
	self.stunTurns = 0;
	$Sprite.texture = idleSprites[spriteRotation];
	
func _on_new_turn(teamIndexTurn):
	self.movedThisTurn = false;
	if teamIndexTurn == self.teamIndex:
		self.movePoints = min(movePoints + movePointsPerTurn, maxMovePoints);
		
	if stunTurns > 0:
		stunTurns-=1;
		if stunTurns == 0:
			$Sprite.texture = idleSprites[spriteRotation];		
			$Sprite.z_index = 0;

func update_move_points_label(label):
	label.text = "MovePoints: " + str(movePoints);

func _process(delta):
	pass
	
func stop_moving():
	self.moving = false;
	game.remove_action_object(self);
	$SkateAudioPlayer.stop();
	
func look_at(targetP):
	var dir = (targetP - position).normalized();
	var angle = rad2deg(atan2(-dir.x, dir.y));
	if angle < 0:
		angle += 360;
	spriteRotation = int(angle/90);
	$Sprite.texture = idleSprites[spriteRotation];
	if puck != null:
		puck.position = position + puckOffsets[spriteRotation];

func _physics_process(delta):
	if firstMoveUpdate:
		for area in get_overlapping_areas():
			handleCollisionWithArea(area);
		firstMoveUpdate = false;
	if moving:
		var dp = 300*delta;
		if position.distance_squared_to(destination) < dp*dp:
			position = destination;
			stop_moving();
		else:
			look_at(destination);
			var dir = (destination - position).normalized();
			var result = get_overlapping_bodies();
			var newPos = position + dir * dp; 
			position = newPos;
		
			
		if puck != null:
			puck.position = position + puckOffsets[spriteRotation];
	
func get_puck_start_position_towards_target(targetP):
	var dir = (targetP - position).normalized();
	var angle = rad2deg(atan2(-dir.x, dir.y));
	if angle < 0:
		angle += 360;
	var spriteRotation = int(angle/90);
	return position + puckOffsets[spriteRotation];
	
func shoot(target):
	if game.doingAction || !self.can_move() || self.puck == null: 
		return;
	$ShootAudioPlayer.stream = shootAudio;
	$ShootAudioPlayer.play();
	look_at(target);
	var puck = self.puck;
	var shootVector = (target - puck.position);
	var shootLength = shootVector.length();
	var shootDir = shootVector / shootLength;
	var velocity = shootDir * (shootLength + 400);
	self.puck = null;
	self.movePoints -= 1.0;
	self.movedThisTurn = true;
	puck.shoot(velocity);
	
func move_to(position):
	if game.doingAction || !can_move(): 
		return;
		
	position = get_node("../../Navigation2D").get_closest_point(position);
	game.add_action_object(self);
	self.moving = true;
	self.firstMoveUpdate = true;
	self.destination = position;
	self.movePoints -= 1.0;
	self.movedThisTurn = true;
	$SkateAudioPlayer.play();
		
func get_move_range():
	return 200;
		
func can_move():
	return !is_stunned() && !movedThisTurn && movePoints >= 1.0 && !moving;

func is_stunned():
	return stunTurns > 0;

func stun():
	stunTurns = 3;
	$Sprite.texture = stunnedSprite;
	$Sprite.z_index = -1;
	
	$ShootAudioPlayer.stream = tackleAudio;
	$ShootAudioPlayer.play();

func _on_area_entered(area):
	handleCollisionWithArea(area);
	
func is_moving_towards(target):
	var moveDir = (destination - position).normalized();
	var areaDir = (target - position).normalized();
	var dot = moveDir.dot(areaDir);
	return dot > 0.95;
	
func handleCollisionWithArea(area):
	if area.get_collision_layer_bit(2) && !is_stunned():
		if !area.owned:
			self.puck = area;
			
	if area.get_collision_layer_bit(1) && moving:
		if area.teamIndex != self.teamIndex && !area.is_stunned():
			if is_moving_towards(area.position) && area.is_in_group("Goalie"):
				stop_moving();
				self.stun();
			if self.puck != null:
				var p = self.puck;
				self.puck = null;
				area.puck = p;
				return;
			else:
				if is_moving_towards(area.position):
					if self.movePoints >= 1.0:
						area.stun();
						self.movePoints -= 1.0;
					else:
						stop_moving();
						self.stun();
		
		if !is_stunned() && area.puck != null:
			var puck = area.puck;
			area.puck = null;
			self.puck = puck;
		
	
