extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	var poly = $CollisionPolygon2D.polygon;
	var newPoly = [];
	for p in poly:
		newPoly.insert(0, p);
	$CollisionPolygon2D.polygon = newPoly;

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
