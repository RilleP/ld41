extends Node2D

onready var game = get_tree().get_root().get_node("Game");

export(int) var teamIndex = 1;
export(bool) var active = true;

var myGoal;
var targetGoal;

var myPlayers = [];
var enemyPlayers = [];

var myGoalie;
var enemyGoalie;

var puck;

var attackGoalVector;
var attackDir;
var targetGoalX;
var attackXMin;
var attackXMax;
var attackYMinOffset = 80.0;
var attackYMaxOffset = 150.0;

var defendGoalVector;
var defendDir;

var myTurn = false;
var doMoveWaitTime = 0.0;
var doMoveDelay = 1.1;

func _ready():
	var goals = get_tree().get_nodes_in_group("Goal");
	for g in goals:
		if g.teamIndex == teamIndex:
			targetGoal = g;
		else:
			myGoal = g;
			
	var goalies = get_tree().get_nodes_in_group("Goalie");
	for g in goalies:
		if g.teamIndex == teamIndex:
			myGoalie = g;
		else:
			enemyGoalie = g;
	
	var players = get_tree().get_nodes_in_group("Outplayer");
	for p in players:
		if p.teamIndex == teamIndex:
			myPlayers.append(p);
		else:
			enemyPlayers.append(p);
			
	puck = get_tree().get_nodes_in_group("Puck")[0];
	
	attackGoalVector = targetGoal.position - myGoal.position;
	attackDir = attackGoalVector.normalized();
	targetGoalX = targetGoal.position.x;
	attackXMin = targetGoalX - attackDir.x * 120;
	attackXMax = targetGoalX - attackDir.x * 70;
	
	defendGoalVector = myGoal.position - targetGoal.position;
	defendDir = defendGoalVector.normalized();
	
	game.connect("on_new_turn", self, "_on_new_turn");
	game.connect("on_remaining_moves_changed", self, "_on_remaining_moves_changed");
	
func get_random_player():
	return myPlayers[randi()%myPlayers.size()];
	
func can_move_player(player):
	return game.curTeamIndexTurn == teamIndex && player.can_move();
	
func p_vector(vector):
	return Vector2(-vector.y, vector.x);
	
func _process(delta):
	if myTurn:
		if doMoveWaitTime > 0:
			doMoveWaitTime -= delta;
			if doMoveWaitTime <= 0:
				do_move();
				
func do_move():
	if !puck.owned:
		var closestP;
		var closestDist;
		for p in myPlayers:
			if !p.can_move():
				continue;
			var dist = p.position.distance_to(puck.position);
			if closestP == null || dist < closestDist:
				closestP = p;
				closestDist = dist;
		
		if closestP != null && closestDist <= closestP.get_move_range():
			move_player_to(closestP, puck.position);
			return;
	
	var enemyHasPuck = false;
	var puckOwner = puck.ownedBy;
	if puckOwner != null:
		print("Puck owner do stuff");
		if puckOwner.teamIndex != self.teamIndex:
			enemyHasPuck = true;
			
			var freeShotTarget = get_free_shot_target_at_goal(puckOwner, myGoal, defendDir);
			if freeShotTarget != null:
				if puckOwner.position.y > myGoal.position.y:
					if myGoalie.move(1):
						return;
				else:
					if myGoalie.move(-1):
						return;
			
			if puckOwner != enemyGoalie:
				var closestP;
				var closestDist;
				for p in myPlayers:
					if !p.can_move():
						continue;
					var pathBlockedBy = path_is_blocked_by(p.position, puckOwner.position);
					if pathBlockedBy != null && pathBlockedBy != puckOwner:
						continue;
						
					var dist = p.position.distance_to(puckOwner.position);
					if closestP == null || dist < closestDist:
						closestP = p;
						closestDist = dist;
				
				if closestP != null && closestDist <= closestP.get_move_range():
					var vector = (puckOwner.position - closestP.position);
					var p = puckOwner.position + p_vector(vector.normalized())*rand_range(30, 50)*random_one();
					var dir = (p - closestP.position);
					#var dir = .rotated(PI*0.15);
					move_player_dir(closestP, dir + dir.normalized() * closestP.get_move_range()*0.5);
					#move_player_to_and_beyond(closestP, puckOwner.position);
					return;
		elif puckOwner.can_move():
			#shot at goal if can score
			var freeShotTarget = get_free_shot_target(puckOwner);
			if freeShotTarget != null:
				puckOwner.shoot(freeShotTarget);
				return;
				
			var freePlayers = find_players_with_free_shot(myPlayers, targetGoal, attackDir);
			if !freePlayers.empty():
				for p in freePlayers:
					var pathBlockedBy = path_is_blocked_by(p.position, puckOwner.position);
					if pathBlockedBy != null && pathBlockedBy != puckOwner:
						continue;
						
					puckOwner.shoot(p.position);
					return;
			
			var p = puckOwner;
			if p.is_in_group("Goalie"):
				var closestP;
				var closestDist;
				for p in myPlayers:
					if !p.can_move() || p == puckOwner:
						continue;
					var pathBlockedBy = path_is_blocked_by(p.position, puckOwner.position);
					if pathBlockedBy != null && pathBlockedBy != puckOwner:
						continue;
	
					var dist = p.position.distance_to(puckOwner.position);
					if closestP == null || dist < closestDist:
						closestP = p;
						closestDist = dist;
	
				if closestP != null:
					puckOwner.shoot(closestP.position);
					return;
			else:
				var attackP = Vector2(rand_range(attackXMin, attackXMax), targetGoal.position.y - rand_range(-attackYMaxOffset, attackYMaxOffset))
				var attackVector = (attackP - p.position)
				var dist = attackVector.length();
				if dist > 100:
					attackP = p.position + attackVector.normalized() * min(p.get_move_range(), dist);
					var pathBlockedBy = path_is_blocked_by(p.position, attackP);
					if pathBlockedBy != null:
						attackVector = attackVector.rotated(PI*rand_range(0.1, 0.15)*random_one());
						attackP = p.position + attackVector;
					
					move_player_to(p, attackP);
					return;
			
#			
	
	
	if !enemyHasPuck:
		#move players towards attack position
		var topAttackCorner = Vector2(attackXMax, targetGoal.position.y - attackYMaxOffset);
		var botAttackCorner = Vector2(attackXMax, targetGoal.position.y + attackYMaxOffset);
		
		var topAttacker = get_closest_player(myPlayers, topAttackCorner, botAttackCorner);
		var topAttackerDist = topAttacker.position.distance_to(topAttackCorner);
		var botAttacker = get_closest_player(myPlayers, botAttackCorner, topAttackCorner);
		var botAttackerDist = botAttacker.position.distance_to(botAttackCorner);
		
		if topAttacker != null && topAttacker.can_move() && topAttackerDist > botAttackerDist:
			var p = topAttacker;
			var attackP = Vector2(rand_range(attackXMin, attackXMax), targetGoal.position.y - rand_range(attackYMinOffset, attackYMaxOffset))
			var attackVector = (attackP - p.position)
			var dist = attackVector.length();
			if dist > 100:
				attackP = p.position + attackVector.normalized() * min(p.get_move_range(), dist);
				var pathBlockedBy = path_is_blocked_by(p.position, attackP);
				if pathBlockedBy != null:
					attackVector = attackVector.rotated(PI*rand_range(0.1, 0.15)*random_one());
					attackP = p.position + attackVector;
				
				move_player_to(p, attackP);
				return;
		if botAttacker != null && botAttacker.can_move():
			var p = botAttacker;
			var attackP = Vector2(rand_range(attackXMin, attackXMax), targetGoal.position.y + rand_range(attackYMinOffset, attackYMaxOffset))
			var attackVector = (attackP - p.position)
			var dist = attackVector.length();
			if dist > 100:
				attackP = p.position + attackVector.normalized() * min(p.get_move_range(), attackVector.length());
				var pathBlockedBy = path_is_blocked_by(p.position, attackP);
				if pathBlockedBy != null:
					attackVector = attackVector.rotated(PI*rand_range(0.1, 0.15)*random_one());
					attackP = p.position + attackVector;
				
				move_player_to(p, attackP);
				return;	
	else:
		#move players towards defense position
		var defender = get_defender();
		if defender != null:
			var p = defender;
				
			var targetP = Vector2(myGoal.position.x + rand_range(80, 120)*-attackDir.x, myGoal.position.y - rand_range(-attackYMaxOffset, attackYMaxOffset))
			var targetVector = (targetP - p.position)
			var dist = targetVector.length();
			if dist > 100:
				targetP = p.position + targetVector.normalized() * min(p.get_move_range(), dist);
				var pathBlockedBy = path_is_blocked_by(p.position, targetP);
				if pathBlockedBy != null:
					targetVector = targetVector.rotated(PI*rand_range(0.1, 0.15)*random_one());
					targetP = p.position + targetVector;
				
				move_player_to(p, targetP);
				return;
	
	for p in myPlayers:
		if !p.can_move():
			continue;
				
		var targetP = Vector2(targetGoal.position.x + rand_range(80, 120)*attackDir.x, targetGoal.position.y - rand_range(-attackYMaxOffset, attackYMaxOffset))
		var targetVector = (targetP - p.position)
		var dist = targetVector.length();
		if dist > 100:
			targetP = p.position + targetVector.normalized() * min(p.get_move_range(), dist);
			var pathBlockedBy = path_is_blocked_by(p.position, targetP);
			if pathBlockedBy != null:
				targetVector = targetVector.rotated(PI*rand_range(0.1, 0.15)*random_one());
				targetP = p.position + targetVector;
			
			move_player_to(p, targetP);
			return;
	
	#if cant make move
	game.force_end_turn(self.teamIndex);
	
func get_defender():
	var closest;
	var bestDist;
	var defenderCount = 0;
	for p in myPlayers:
		if !p.can_move():
			continue;
		var dist = abs(p.position.x - myGoal.position.x);
		if dist < 200:
			defenderCount += 1;
			if defenderCount >= 2:
				return null;
			continue;
		if closest == null || dist < bestDist:
			closest = p;
			bestDist = dist;
	
	return closest;
	
func get_closest_player(players, position, otherPos):
	var closest;
	var bestDist;
	for p in players:
		var dist = p.position.distance_to(position);
		var otherDist = p.position.distance_to(otherPos)
		if dist > otherDist:
			continue;
		if closest == null || dist < bestDist:
			closest = p;
			bestDist = dist;
	return closest;
	
func random_one():
	return (randi()%2)*2 - 1;

func move_player_dir(player, vector):
	var target = player.position + vector;
	player.move_to(get_legal_move_position_towards(player, target));
	
func move_player_to(player, target):
	player.move_to(get_legal_move_position_towards(player, target));
	
func move_player_to_and_beyond(player, target):
	var v = (target - player.position);
	if v.length() == 0:
		v = Vector2(randf(), randf());
	target = player.position + v.normalized() * player.get_move_range();
	player.move_to(get_legal_move_position_towards(player, target));
	
func get_legal_move_position_towards(player, target):
	var vector = (target - player.position);
	return player.position + vector.clamped(player.get_move_range());
	
func path_is_blocked_by(from, to):
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_ray(from, to, myPlayers, 1 << 1)
	if result.empty():
		return null;
	else:
		return result["collider"];
		
func find_players_with_free_shot(players, goal, attackDir):
	var result = [];
	for p in players:
		if !p.can_move():
			continue;
		if get_free_shot_target_at_goal(p, goal, attackDir):
			result.append(p);
	return result;

func get_free_shot_target(player):		
	return get_free_shot_target_at_goal(player, targetGoal, attackDir);
func get_free_shot_target_at_goal(player, goal, attackDir):
	var space_state = get_world_2d().direct_space_state
	
	var goalTargetPositions = [
		Vector2(0, 38),
		Vector2(0, -38),
		Vector2(0, 30),
		Vector2(0, -30),
		Vector2(0, -20),
		Vector2(0, 20),
		Vector2(0, -10),
		Vector2(0, 10),
		Vector2(0, 0),
	]
	
	for offset in goalTargetPositions:
		var tp = goal.position + offset;
		tp.x -= attackDir.x*20;
		print(tp);
		var puckPosition = player.get_puck_start_position_towards_target(tp);
		var result = space_state.intersect_ray(puckPosition, tp, [self], 1 << 1)
		if result.empty():
			print("Found free shot")
			return tp;
		else:
			print("Goal blocked by " + result["collider"].name);
	return null;
	
func _on_remaining_moves_changed(teamIndex, remainingMoves):
	if !active:
		return;
	if game.curTeamIndexTurn != self.teamIndex || remainingMoves <= 0:
		return;
	
	if doMoveWaitTime <= 0:
		doMoveWaitTime = doMoveDelay;
	
	
func _on_new_turn(teamIndexTurn):
	myTurn = teamIndexTurn == teamIndex;
	
