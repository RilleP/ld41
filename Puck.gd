extends Area2D

onready var game = get_tree().get_root().get_node("Game");

var moving = false;
var velocity;

var radius;

var owned = false setget ,_get_owned;
func _get_owned():
	return ownedBy != null;

var ownedBy = null setget _set_owner;

func _set_owner(newValue):
	ownedBy = newValue;

var didScore = false;

var givePuckToGoalie;
var givePuckToGoalieTimer;
var givePuckToGoalieDelay = 2.0;

onready var startPosition = position;

func _ready():
	radius = $CollisionShape2D.shape.radius;
	
func reset():
	self.position = startPosition;
	self.didScore = false;
	
func shoot(velocity):
	
	if game.doingAction: 
		return;
	game.add_action_object(self);
	self.velocity = velocity;
	moving = true;
	owned = false;
	
func _process(delta):
	if givePuckToGoalie != null:
		givePuckToGoalieTimer-=delta;
		if givePuckToGoalieTimer <= 0:
			givePuckToGoalie.puck = self;
			givePuckToGoalie = null;
	
func _physics_process(delta):
	if moving:
		var speed = velocity.length();
		var speedDec = (speed*0.1 + 300)*delta;
		if speedDec > speed:
			moving = false;
			game.remove_action_object(self);
			
			if !game.paused:
				var goals = get_tree().get_nodes_in_group("Goal")
				for g in goals:
					if g.get_node("BehindArea").overlaps_area(self):
						if g.goalie != null:
							$WhistlePlayer.play();
							givePuckToGoalie = g.goalie;
							givePuckToGoalieTimer = givePuckToGoalieDelay;
					
			
			return;
		
		speed -= speedDec;
		velocity = velocity.normalized() * speed
		var newPos = position + velocity*delta; 			
		var space_state = get_world_2d().direct_space_state
		var result = space_state.intersect_ray(position, newPos, [self], 1 << 3)
		if result.empty():
			position = newPos;
		else:
			var normal = result["normal"];
			var bounce = result["collider"].bounce;
			if didScore:
				bounce *= 0.2;
			velocity = velocity.bounce(normal) * bounce;
			
		
