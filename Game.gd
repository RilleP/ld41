extends Node2D

var score = [0, 0];

var gameOver = false;
var paused = false;

var resetTime = 0.0;
var goalResetDelay = 3.0;

var doingAction setget ,doingActionGet;
func doingActionGet():
	return !curActionObjects.empty();

var curActionObjects = [];

const teamCount = 2;

var curTeamIndexTurn = -1;
var curTeamMovesRemaining = 0;

var teamBaseMovesPerTurn = 2;

var curTurnIndex = 0 setget _set_curTurnIndex;

func _set_curTurnIndex(newValue):
	curTurnIndex = newValue;
	get_node("UI/TurnLabel").text = str(curTurnIndex+1) + "/" + str(maxTurnCount);

var maxTurnCount = 60;

var curTeamGoalieHasPuck;

var goalies;

var activate_both_ai = null;

func set_ai_active(active):
	var ais = get_tree().get_nodes_in_group("AI");
	for ai in ais:
		ai.active = active;

func on_goal(teamIndex):
	if paused:
		return;
	paused = true;
	score[teamIndex] += 1;
	update_score_ui();
	
	if curTurnIndex > maxTurnCount && !score_is_even():
		game_over();
		return;
	resetTime = goalResetDelay;
	$GameAudioPlayer.play();

signal on_new_turn(teamIndex);
signal on_remaining_moves_changed(teamIndex, remainingMoves);

func update_score_ui():
	get_node("UI/Team1ScoreLabel").text = "Blue " + str(score[0]); 
	get_node("UI/Team2ScoreLabel").text = str(score[1]) + " Red";
	
func _input(event):
	if event is InputEventKey:
		if event.is_pressed() && event.scancode == KEY_ESCAPE:
			toggle_menu_dialog();
		
			
func toggle_menu_dialog():
	var dialog = get_node("UI/MenuDialog");
	dialog.visible = !dialog.visible;
	
func game_over():
	$GameAudioPlayer.play();
	gameOver = true;
	var dialog = get_node("UI/GameOverDialog");
	dialog.visible = true;
	
	var winnerTeamIndex = 0;
	if score[0] < score[1]:
		winnerTeamIndex = 1;
	elif score[0] > score[1]:
		winnerTeamIndex = 0;
	var winnerScore;
	
	
	var winText = "";
	if activate_both_ai != null:
		if winnerTeamIndex == 0:
			winText = "Blue team won!";
		else:
			winText = "Red team won!";
	else:
		if winnerTeamIndex == 0:
			winText = "You won!";
		else:
			winText = "You lost!";
	
	dialog.get_node("Title").text = winText;
	dialog.get_node("Info").text = "Final score is\nBlue " + str(score[0]) + " - " + str(score[1]) + " Red";
	
func score_is_even():
	return score[0] == score[1];
	
func increase_turn():
	curTeamIndexTurn += 1;
	var newTurnIndex = curTurnIndex;
	if curTeamIndexTurn == teamCount:
		newTurnIndex = curTurnIndex + 1;
		curTeamIndexTurn = 0;
		
	if newTurnIndex >= maxTurnCount && !score_is_even():
		game_over();
		return;
	self.curTurnIndex = newTurnIndex;
		
	curTeamMovesRemaining = teamBaseMovesPerTurn;
	emit_signal("on_remaining_moves_changed", curTeamIndexTurn, curTeamMovesRemaining);
	emit_signal("on_new_turn", curTeamIndexTurn);
	
	update_goalie_has_puck();
	
	var anim;
	if randf() < 0.5:
		anim = "sweep"
	else:
		anim = "turn"
	
	get_node("UI/NewTurnPanel/AnimationPlayer").play(anim);

func update_goalie_has_puck():
	curTeamGoalieHasPuck = false;
	for g in goalies:
		if g.teamIndex == curTeamIndexTurn && g.puck != null:
			curTeamGoalieHasPuck = true;

func add_action_object(object):
	if curActionObjects.empty():
		curTeamMovesRemaining -= 1;
		emit_signal("on_remaining_moves_changed", curTeamIndexTurn, curTeamMovesRemaining);
		
	curActionObjects.append(object);
	
func remove_action_object(object):
	
	while true:
		var index = curActionObjects.find(object);
		if index == -1:
			break;
		curActionObjects.remove(index);
		
	update_goalie_has_puck();
		
	print("moves remaining: " + str(curTeamMovesRemaining));
	if curActionObjects.empty() && curTeamMovesRemaining <= 0 && !paused:
		increase_turn();
	
func team_can_move(teamIndex):
	return !paused && curTeamIndexTurn == teamIndex && curTeamMovesRemaining > 0;
	
	
func _ready():
	if activate_both_ai != null:
		set_ai_active(activate_both_ai);
	goalies = get_tree().get_nodes_in_group("Goalie");
	increase_turn();
	update_score_ui();
	self.curTurnIndex = 0;

func restart_game():
	score = [0, 0];
	gameOver = false;
	self.curTurnIndex = 0;
	curTeamIndexTurn = -1;
	update_score_ui();
	reset_round();

func reset_round():
	var resetables = get_tree().get_nodes_in_group("Resetable");
	for r in resetables:
		r.reset();
		
	self.paused = false;
	increase_turn();

func _process(delta):
	if gameOver:
		return;
	if paused:
		resetTime -= delta;
		if resetTime <= 0:
			reset_round();

func _on_EndTurnButton_pressed():
	force_end_turn(get_node("Input").myTeamIndex);
		
func force_end_turn(teamIndex):
	if curTeamIndexTurn != teamIndex:
		return;
		
	if curTeamGoalieHasPuck && curTeamMovesRemaining > 0:
		show_warning("Must shoot with goalie");
		return;
	
	if doingAction:
		curTeamMovesRemaining = 0;
	else:
		increase_turn();
		
func show_warning(text):
	var panel = get_node("UI/WarningPanel");
	panel.get_node("AnimationPlayer").play("fade_inout");
	panel.get_node("Label").text = text;


func _on_QuitToMenuButton_pressed():
	queue_free();
	get_tree().change_scene("res://MainMenu.tscn");

func _on_HideMenuDialogButton_pressed():
	toggle_menu_dialog();


func _on_PlayAgainButton_pressed():
	var dialog = get_node("UI/GameOverDialog");
	dialog.visible = false;
	restart_game();


func _on_MenuButton_pressed():
	toggle_menu_dialog();
