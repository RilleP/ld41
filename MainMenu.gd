extends Node2D


func _on_PlayVsAiButton_pressed():
	queue_free();
	var game = preload("res://Game.tscn").instance();
	game.get_node("Input").multiplayer = false;
	get_node("/root").add_child(game)

func _on_AiVsAiButton_pressed():
	queue_free();
	var game = preload("res://Game.tscn").instance();
	game.get_node("Input").multiplayer = false;
	game.activate_both_ai = true;
	get_node("/root").add_child(game)
	


func _on_QuitButton_pressed():
	if OS.get_name() == "HTML5":
		get_node("WebCloseDialog").show_on_top = true;
		get_node("WebCloseDialog").show();
	else:
		get_tree().quit();


func _on_MultiplayerButton_pressed():
	queue_free();
	var game = preload("res://Game.tscn").instance();
	game.get_node("Input").multiplayer = true;
	game.activate_both_ai = false;
	get_node("/root").add_child(game)


func _on_HelpButton_pressed():
	queue_free();
	get_tree().change_scene("res://HelpMenu.tscn");
