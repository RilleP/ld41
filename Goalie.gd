extends Area2D

onready var game = get_tree().get_root().get_node("Game");

var goal;
var positionIndex = 1 setget set_positionIndex;

func flipPos(pos):
	if xFlipped:
		pos.x *= -1;
	return pos;
	

func set_positionIndex(newValue):
	positionIndex = newValue;
	update_texture();
	var goalRotation = goal.rotation - PI/2;
	position = goal.position + flipPos(positionOffsets[positionIndex]);
	print(goal.rotation)
	if puck != null:
		puck.position = position + flipPos(puckOffsets[positionIndex]);

var puck setget set_puck;

var movedThisTurn = false;

var puckOffsets = [
	Vector2(-20, 0),
	Vector2(-20, 0),
	Vector2(-20, 0),
]

var positionOffsets = [
	Vector2(-40, -30),
	Vector2(-70, 0),
	Vector2(-40, 30),
]

export(int) var teamIndex = 0;

var spritesBlue = [
	preload("res://goalie/idle_up.png"),
	preload("res://goalie/idle_fwd.png"),
	preload("res://goalie/idle_down.png"),
]

var spritesRed = [
	preload("res://goalie/red_idle_up.png"),
	preload("res://goalie/red_idle_fwd.png"),
	preload("res://goalie/red_idle_down.png"),
]

var sprites;

var xFlipped = false;

func set_puck(newValue):
	if puck != null:
		puck.ownedBy = null;
	puck = newValue;
	if puck != null:
		puck.ownedBy = self;
		if puck.moving:
			puck.moving = false;
			game.remove_action_object(puck);
			
		puck.position = position + flipPos(puckOffsets[positionIndex]);

func _ready():
	var goals = get_tree().get_nodes_in_group("Goal")
	
	var bestGoal = null;
	var bestDist;
	for g in goals:
		var dist = g.position.distance_to(position);
		if bestGoal == null || dist < bestDist:
			bestGoal = g;
			bestDist = dist;
	goal = bestGoal;
	goal.goalie = self;
	
	xFlipped = goal.rotation < 0;
	$Sprite.flip_h = xFlipped;
	
	if teamIndex == 0:
		sprites = spritesBlue;
	else:
		sprites = spritesRed;
	
	self.positionIndex = 1;
	game.connect("on_new_turn", self, "_on_new_turn");
	
func reset():
	self.positionIndex = 1;
	self.puck = null;
	self.movedThisTurn = false;
	
func _on_new_turn(teamIndexTurn):
	self.movedThisTurn = false;
	
func update_move_points_label(label):
	label.text = "";

func update_texture():
	$Sprite.texture = sprites[positionIndex];
	
func move(deltaPos):
	if game.doingAction || !self.can_move():
		return false;
	
	var newPos = clamp(positionIndex + deltaPos, 0, 2);
	if newPos == positionIndex:
		return false;
	
	game.add_action_object(self);
	self.positionIndex = newPos;
	self.movedThisTurn = true;
	game.remove_action_object(self);
	return true;
	
func get_puck_start_position_towards_target(targetP):
	return position + flipPos(puckOffsets[positionIndex]);

func shoot(target):
	if game.doingAction || !self.can_move() || self.puck == null: 
		return;
	$ShootAudioPlayer.play();
	var puck = self.puck;
	var velocity = (target - puck.position)*3;
	self.puck = null;
	self.movedThisTurn = true;
	puck.shoot(velocity);

func get_move_range():
	return -1;

func can_move():
	return !movedThisTurn;
	
func is_stunned():
	return false;

func stun():
	pass
	
func _on_area_entered(area):
	if area.get_collision_layer_bit(1):
		if area.puck != null:
			var puck = area.puck;
			area.puck = null;
			self.puck = puck;
	if area.get_collision_layer_bit(2):
		if !area.owned:
			self.puck = area;
